$(document).ready(function() {
  
  mapImageHandler = {
    
    
    mouseOver:function(region) {

      
    }
    
    
  }
  mapMovieHandler = {
    regions:['us', 'asia', 'europe', 'switzerland', 'emma', 'global', ''],
    regionIDs:[29, 27, 26, 25, 28, 30, -1],
    setSelected:function(id) {
			var inArray = false;
			for(var i = 0; i < this.regionIDs.length; i++) {
				if(id == this.regionIDs[i]) {
					inArray = true;
					break;
				}
				
			}
			if(!inArray) {
				return;
			}
      var region = this.getRegionFromColID(id);
      if(hasFlash) {
        var el = getMapMovie();
        el.setSelected(region);
			} else {
				maplinkHandler.selectFromButtons(region);
			}
      
      
    },
    setSelectedFromMapMovie:function(region) {
      
      var id = this.getColIDFromRegion(region);
      buttonManager.mapClicked(id);
      
      
      
    },
    
    
    getColIDFromRegion:function(region) {
      for(var i = 0; i < this.regions.length; i++) {
        
        if(region == this.regions[i]) {
          return this.regionIDs[i];
        }
      }
      return 0;
      
    },
    getRegionFromColID:function(id) {
      
      for(var i = 0; i < this.regionIDs.length; i++) {
        if(id == this.regionIDs[i]) {
          
          return this.regions[i];
        }
      }
      return '';
    }
    
    
    
  }
  maplinkHandler = {
  
    mapNames:['us', 'switzerland','emma','asia', 'europe', 'global', ''],
  
  
   select:function(region) {
    this.selectFromButtons(region);

    mapMovieHandler.setSelectedFromMapMovie(region);
     
   },
	  selectFromButtons:function(region) {
			
			var topValue = $('#'+region+'-selected').css("top");
		
		  if(topValue == '0px' ) {
	      region = '';
      }
		
		 for(var i = 0; i < this.mapNames.length; i++) {
       if(region == this.mapNames[i]) {
        
        $('#'+region+'-selected').css('top', '0');
				
       } else {
        $('#'+this.mapNames[i]+'-selected').css('top', '-50000px');
       }
      
      
     }
		
	 },
   mIn:function(region) {
    
    for(var i = 0; i < this.mapNames.length; i++) {
       if(region == this.mapNames[i]) {
        
        $('#'+region+'-over').css('top', '0');
       } else {
        $('#'+this.mapNames[i]+'-over').css('top', '-50000px');
       }
      
      
     }
   },
   mOut:function(region) {
    
    for(var i = 0; i < this.mapNames.length; i++) {
       
        $('#'+this.mapNames[i]+'-over').css('top', '-50000px');
       
      
      
     }
    
   }
  
  
  
 },
 mapCategoryHandler = {
	 shouldProcessClick:function(id) {
		 
		
		
		
		
		
	 }
 }

  
  
});
var mapImageHandler = null;
var mapMovieHandler = null;
var maplinkHandler = null;
var mapCategoryHandler = null;
function getMapMovie() {
		try {
      if (navigator.appName.indexOf("Microsoft") != -1) {
        return window['mapmovie'];
      } else {
				
        return document['mapmovie'];
      }
    } catch(e1) {
      
    }
		return '';
	}