/*********************************************************************************
@autor J.Klingler
According to the implementation of Lenar Zakirov

It is in fact easy to use ajax. But remember to give a message to the
user when an ajax request is running and not already answered 
how to use this:

<script src="ajax.js"></script>

<script>
ajaxObj=new Ajax(url) 
ajaxObj.addParam(key,value);
ajaxObj.setMemory(obj);  // is available in callback function as second parameter
ajaxObj.setCallBack('functionname'); // use a string representation of the function name, not the function reference
ajaxObj.send()


// and if you like, also:
ajaxObj.inputEx('input_id') // adds the value of the given input element as parameter
ajaxObj.setType('http')  // http is default , maybe you want 'xml'
ajaxObj.inputObj(obj) // the input object as parameter



function functionname(answerFromServer [, memory_obj ]){
  // do what you like

}
</script>
**********************************************************************************/



ajaxLinkId=0
ajaxLinks=new Array();
ajaxLinkTimeOut=10000;

Ajax = function(url) {
  this.url=url
  this.params='';
  this.type='http'
  this.callBackFunction
  this.loaded=false
  this.memory=null;
  this.sync=true
  this.linkId=ajaxLinkId
  ajaxLinks[ajaxLinkId]=this
  ajaxLinkId++
  
}
Ajax.prototype.setMemory = function(obj) {
  this.memory=obj 
}
Ajax.prototype.addParam = function(key,val) {
 this.params=this.params+'&'+key+'='+ escape(val) 
  
}
Ajax.prototype.setSync= function(bool) {
  this.sync=bool; 
  
}
Ajax.prototype.inputEx = function(inputId) {
  var val=null;
  try {
    var el=document.getElementById(inputID);
    if((el.type !== undefined)&&(el.type == 'ckeckbox'))  {
      val=el.checked ? "1":"0";
      
    } else {
    
    
      var val=document.getElementById(inputId).value
    }
    var name=document.getElementById(inputId).name
  } catch(e) {
    alert(e)
  }
  this.addParam(name,val);
}
Ajax.prototype.inputObj = function(obj) {
 this.addParam(obj.name,obj.value); 
  
  
}


Ajax.prototype.send= function() {
  
  var x
  try {x=new ActiveXObject("Msxml2.XMLHTTP")}
  catch (e) {
    try {x=new ActiveXObject("Microsoft.XMLHTTP")}
    catch (e) {x=null}
  }
  if (!x && typeof XMLHttpRequest!="undefined") x=new XMLHttpRequest()
  if (!x) {
    this.error("Could not create connection object!")
    return
  }
  linkid=this.linkId
  type=this.type
  x.onreadystatechange=function(){
    
    if(x.readyState==4) {
      
      switch (type){
        
        case "http":
          ajaxLinks[linkid].callBack(x.responseText)
          break;
        case "xml":
          ajaxLinks[linkid].callBack(x.responseXML)
          break;
      }
    }
  }
  x.open("POST", this.url, this.sync); 
  //window.setTimeout("ajaxLinks["+this.linkId+"].timeOut()",ajaxLinkTimeOut)
  x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  x.setRequestHeader("Content-length", this.params.length);
  x.send(this.params)
}
Ajax.prototype.callBack = function(retVal){
  if (!this.loaded) {
    this.loaded=true
    ajaxManager.stopWait()
  }
  if(this.callBackFunction) {
    if(this.memory) {
      var mem=this.memory
      eval("this.callBackFunction(retVal,mem)")
    } else {
      eval("this.callBackFunction(retVal)")
    }
  }
  delete ajaxLinks[this.linkId]
}
Ajax.prototype.setCallBack = function(functionName) {
  if(this.memory != null) {
    mem=this.memory
    this.callBackFunction = new Function("retVal",""+functionName+"(retVal,mem)")
  } else {
    this.callBackFunction = new Function("retVal",""+functionName+"(retVal)")
  }
}


Ajax.prototype.timeOut = function(){
  if (!this.loaded) this.error("Server connection timeout!")
}
Ajax.prototype.error = function(text){
  this.callBack=null
  this.loaded=true
  ajaxManager.error(text)
}

ajaxManager={
  stopWait:function() {
    
  },
  startWait:function() {
    
  },
  error:function(text) {
    alert(text); 
  }
}





