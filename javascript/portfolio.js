$(document).ready(function() {
  var dType = 'xml';
	
	if ( $.browser.msie ) {
		
		dType = 'xml';
		
  } else {
    dType = 'text';
  }
	
  $.ajax({
    type: "GET",
	  url: "ubs_g15_cio_shelf_short-list_v4.xml",
	  dataType: dType,
		contentType: "application/xml;charset=UTF-8",
	  success: function(xml) {
			
			
      var $xml = $(xml);
      var title = $xml.find("Author").text();

			
		  rows = $xml.find("Row");
			var cells = $(rows[0]).children();

			row0 = [];
			row1 = [];
			var startIndex = 0;
			var endIndex = 0;
			var merges = 0;
			var na = '';
			$(rows[0]).children().each(function() {

				if($(this).attr('ss:MergeAcross')) {
			    merges = parseInt($(this).attr('ss:MergeAcross'));
				} else {
				
					merges = 0;
				}
				
				endIndex = startIndex +merges;
				na = $(this).find('Data').text();
				var validSection = 0;
				if(na.length > 0 && merges > 0) {
					validSection = 1;
				}
			
			 	row0.push({name:na,  merge:merges, selectedButtons:[], categoryIndex:row0Counter, startIndex:startIndex, endIndex:endIndex, validSection:validSection, selectionChecked:0});
				startIndex = endIndex;
				startIndex++;
				
				
			  row0Counter++;
			});
			
			
			var languageCat = row0[CATEGORY_INDEX_LANGUAGE];

			$(rows[1]).children().each(function() {
				
				
			  	row1.push($(this).find('Data').text());
				
				
			});
			columnsCount = row1.length;

			var sectionTable;
			var x = '';
			var j = 0;
			
			colObjArray = [];
			var assetClassAppendString = '';
			var frequencyAppendString = '';
			var segmentAppendString = '';
			var regionAppendString = '';
			for(var i = 0; i < row0.length; i++) {
				var colObj = null;
				var na = row0[i].name;
				var mer = row0[i].merge;
				
				if(na.length > 0 && mer > 0) {
					mer++;
					var buttonContainer = null;
					
					for(var ii=0; ii < mer; ii++) {
						
						colObj = {colIndex:j, name: row1[j], cat:i, selected:1}
						colObjArray.push(colObj);
						if(i == 3) {
							segmentAppendString +=   '<li id="list'+j+'" class="unSelectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
						} else	if(i == 4) {
							assetClassAppendString += '  <li id="list'+j+'" class="unSelectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
							//$('#assetClassButtons').append('  <li><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')">'+colObj.name+'</a></li>');
						} else if(i == 6) {
							frequencyAppendString += '  <li id="list'+j+'" class="unSelectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
						} else if(i == CATEGORY_INDEX_REGION) {
							
							regionAppendString += '  <li id="list'+j+'" class="unSelectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
						}
						
						
						j++;
						
					}
					
					
				} else {
					colObj = {colIndex:j, name: row1[j], cat:i, selected:1}
					colObjArray.push(colObj);
					j++;
				}
				
				
				
			}
			//$('#segmentButtons').append(segmentAppendString);
			$('#assetClassButtons').append(assetClassAppendString);
			$('#frequencyButtons').append(frequencyAppendString);
			$('#regionButtonsSub').append(regionAppendString);
			
			allRows = [];
			var rowArr = null;
			var inFocusRows = [];
      var latestRows = [];
      var popularRows = [];
			
			var isInFocus = false;
			var latest = false;
			var popular = false;
			var inFocusIndex = 0;
      var href = '';
			var languagesString;
			for(var iii= 0; iii < rows.length; iii++) {
				languagesString = [];
				isInFocus = false;
			  latest = false;
			  popular = false;
				var validRow = false;
				href = '';
				rowArr = [];
				$(rows[iii]).children().each(function(index) {
				  var data = $(this).find('Data').text();
					// check for row validity
					if(index == PDF_LINK_INDEX) {
						//data = iii;
						if((data.length > 0)&& (data != '?')) {
							
							
							//validRow = true;
						}
						
					}
          if(index == (columnsCount - PDF_REWARD_INDEX)) {
            if($(this).attr('ss:HRef')) {
              validRow = true;
               href = $(this).attr('ss:HRef');
            
            }
            
            
          }
					
					
			  	rowArr.push(data);
					if(index == (columnsCount - LATEST_REWARD_INDEX)) {
						if(data == 'x') {
							latest = true;
							
						}
					}
					if(index == (columnsCount - POPULAR_REWARD_INDEX)) {
						
						if(data == 'x') {
							
							popular = true;
						}
					}
					
					
					
					
					
				
				
			  });
				if(validRow) {
					
					for(var ilc = languageCat.startIndex; ilc <= languageCat.endIndex; ilc++) {
						
						if(rowArr[ilc] == 'x') {
							
							languagesString.push(row1[ilc]);
						}
						
					}
					
					
					
					
					
					rowArr.push(href);
					rowArr.push(languagesString.join(', '))
				  allRows.push(rowArr);
					
					
				
					if(latest) {
						latestRows.push({row:rowArr, focusIndex:inFocusIndex});
					}
					if(popular) {
						popularRows.push({row:rowArr, focusIndex:inFocusIndex});
						
					}
					if(latest || popular) {
						inFocusRows.push(rowArr);
						inFocusIndex++;
					}
				}
				
			}

    
			globalSelectedRows = [];
			buttonManager.displayRows();
			
			
      
    }
		
  });
	buttonManager = {
		
		mapClicked:function(index) {
			categoryManager.setButtonClicked(index);
			this.displayRows();
		},
		
		clicked:function(index) {
			
			mapMovieHandler.setSelected(index);
			categoryManager.setButtonClicked(index);
			this.displayRows();
		},
		displayRows:function() {
			globalSelectedRows = [];
			var currentRow = null;
			var chance = true;
			var selectionFound = false;
			var i = 0;
			var columnLen = 0;
			for(var ii = 0; ii < allRows.length; ii++) {
				currentRow = allRows[ii];
				
				chance = true;
				var cat = null;
				for(i = 0; i < row0.length; i++) {
				  cat = row0[i];
					columnLen = 0;
					selectionFound = false;
				
					if(cat.validSection == 1 && ( cat.categoryIndex == CATEGORY_INDEX_ASSETCLASS  || cat.categoryIndex == CATEGORY_INDEX_FREQUENCY  || cat.categoryIndex == CATEGORY_INDEX_REGION)) {
						
						
						if(cat.selectionChecked == 0) {
              
						  for(var j = cat.startIndex; j <= cat.endIndex; j++) {
							  if(colObjArray[j].selected == 1) {

									cat.selectedButtons.push(j);
								  
								}
								
							}
						
							cat.selectionChecked = 1;
							
							
						}
						
						columnLen = (cat.endIndex - cat.startIndex +1);
						
						if((cat.selectedButtons.length < columnLen)&&(cat.selectedButtons.length > 0)) {
						 	selectionFound = false;
							for(var iii = 0; iii < cat.selectedButtons.length; iii++) {
								if(currentRow[cat.selectedButtons[iii]] == 'x') {
									selectionFound = true;
									
									
								}
								
								
							}
							if(!selectionFound) {
								chance = false;
								
								i = row0.length;  // works as a break statement
								
							}
							
							
						}
						
					}
					
					
					
					
				}
				if(chance) {
				  globalSelectedRows.push(currentRow);
			  }
				
				
				
			}
	
			falseArray = [];
			for(i = 0; i < row0.length; i++) {
			  cat = row0[i];
				if(cat.validSection == 1) {
					cat.selectionChecked = 0;
					cat.selectedButtons = [];
				}
					
			}


			if(globalSelectedRows.length < 1) {
				$('#rightColumnContentList').html('No publications found for the current criteria');
				
			} else {
			 
			  var rowsString = '<ul id="publicationList">';
        var x = globalSelectedRows.length;
			  var descriptionString = '';
			  for(i =0; i < globalSelectedRows.length; i++) {
					currentRow = globalSelectedRows[i];
					
					
					descriptionString = currentRow[columnsCount - DESCRIPTION_REWARD_INDEX ];
					if((undefined != descriptionString) && (descriptionString.length > 0)) {
						
						descriptionString = descriptionString.replace(/(\ufffd|\u0000)/g, "");
					  descriptionString = descriptionString.replace(/\[a (.*?)\]/g, "<a $1 class=\"innerlink\" target=\"_blank\">");
						descriptionString = descriptionString.replace(/\[\/a\]/g, "</a>");
						descriptionString = descriptionString.replace(/(\r\n|\r|\n|\u0085|\u000C|\u2028|\u2029)/g, "<br />");
						
				  }
					
					rowsString += '<li><table class="list-table"><tr><td valign="top" class="image-cell">' +
                '    <img class="table_image" src="images/pdf-preview/'+ currentRow[PDF_LINK_INDEX] +'.jpg">'+
								'  </td><td valign="top"><div class="list-content"><h3>'+currentRow[columnsCount - TITLE_REWARD_INDEX ]+'</h3>'+
                   descriptionString+
	 								'  <div class="document-languages">Languages: '+ currentRow[columnsCount+1]+'</div>'+
                '  <a class="download-button rounded" target="_blank" href="'+currentRow[columnsCount]+'" title="'+currentRow[(columnsCount - PDF_REWARD_INDEX)]+'">Download</a>'+
                '  </div></td></tr></table></li>';
				   
				  
			  }
				
			  
			  rowsString += '</ul>';
			  
				
			  $('#rightColumnContentList').html(rowsString);
    
			  
			}
			setOpenParts('list');
			
			
			
			
			
			
			
			
		}
		
		
		
		
		
		
	}
	categoryManager = {
		
		setButtonClicked:function(id) {
			var catIndex = colObjArray[id].cat;
			
			var cat = row0[catIndex];
			
			if(cat.validSection == 0) {
				return;
			}
			if(cat.categoryIndex == CATEGORY_INDEX_REGION) {
				
				regionCategoryManager.setButtonClicked(id);
				
				return;
			}
			
			
			
			
			var hasUnselectedButton = false;
			
			for(var j = cat.startIndex; j <= cat.endIndex; j++) {
				
			  if(colObjArray[j].selected == 0) {

					
				  hasUnselectedButton = true;
					break;
				}
								
			}
			if(hasUnselectedButton) {
				if(colObjArray[id].selected == 1) {
					
					colObjArray[id].selected = 0;
					var hasSelectedButton = false;
					for(j = cat.startIndex; j <= cat.endIndex; j++) {
						if(colObjArray[j].selected == 1) {
							hasSelectedButton = true;
						} 
					}
					
					if(hasSelectedButton) {
						graficButtons.setUnselected([id], catIndex);
					} else {
						var idArray = [];
						for(j = cat.startIndex; j <= cat.endIndex; j++) {
							colObjArray[j].selected = 1;
					    idArray.push(j);
						}
						graficButtons.setUnselected(idArray, catIndex);
					}
					
				} else {
					colObjArray[id].selected = 1;
					graficButtons.setSelected([id], catIndex);
				}
			} else {
				
				var idArray =  [];
				for(var j = cat.startIndex; j <= cat.endIndex; j++) {
					
				  if(j != id) {
						colObjArray[j].selected = 0;
						idArray.push(j);
					} 
					
					
				}
				
				graficButtons.setUnselected(idArray, catIndex);
				graficButtons.setSelected([id], catIndex);
				
			}
			
			
			
		}
		
		
		
	}
	regionCategoryManager = {
		selectAll:function() {
			this.selectAllFromMap();
			//this.
			mapMovieHandler.setSelected(-1);
		},
		selectAllFromMap:function() {
			var cat = row0[CATEGORY_INDEX_REGION];
			var notSelect = [];
			for(var j = cat.startIndex; j <= cat.endIndex; j++) {
			
				colObjArray[j].selected = 0;
				notSelect.push(j);
			
						
			}
			graficButtons.setUnselected(notSelect, CATEGORY_INDEX_REGION);
			buttonManager.displayRows();
		},
		setButtonClicked:function(id) {
			
			
			var cat = row0[CATEGORY_INDEX_REGION];
			
			
				var hasUnselectedButton = false;
				for(var j = cat.startIndex; j <= cat.endIndex; j++) {
				
			    if(colObjArray[j].selected == 0) {
  				  hasUnselectedButton = true;
					  break;
				  }
				
								
			  }
			  if(hasUnselectedButton) {
				  if(colObjArray[id].selected == 1) {
						var idArray = [];
				    for(j = cat.startIndex; j <= cat.endIndex; j++) {
					    colObjArray[j].selected = 1;
			        idArray.push(j);
				    }
				    graficButtons.setUnselected(idArray, CATEGORY_INDEX_REGION);
					} else {
						var notSelect = [];
					  for(var j = cat.startIndex; j <= cat.endIndex; j++) {
						  if(j != id ) {
								colObjArray[j].selected = 0;
							  notSelect.push(j);
						  }
						
					  }
					  graficButtons.setUnselected(notSelect, CATEGORY_INDEX_REGION);
						colObjArray[id].selected = 1;
					  graficButtons.setSelected([id], CATEGORY_INDEX_REGION);
					}
					
					
					
				} else {
					var notSelect = [];
					for(var j = cat.startIndex; j <= cat.endIndex; j++) {
						if(j != id ) {
							colObjArray[j].selected = 0;
							notSelect.push(j);
						}
						
					}
					graficButtons.setUnselected(notSelect, CATEGORY_INDEX_REGION);
					colObjArray[id].selected = 1;
					graficButtons.setSelected([id], CATEGORY_INDEX_REGION);
					
				}
			
			
			
			
			
			
			
			
			
		}
		
		
	}
	
	
	graficButtons = {
		setUnselected:function(idArray, catIndex) {
			
			
			
			
			
			
			cat = row0[catIndex];
			if(catIndex == CATEGORY_INDEX_REGION) {
				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).removeClass('selectedButton').addClass('unSelectedButton');
				  
				}
				//mapHandler.unselectRegionArray(idArray);
			} else {

				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).removeClass('selectedButton').addClass('unSelectedButton');
				  
				}
			}
		},
		setSelected:function(idArray, catIndex) {
			

			
			if(catIndex == CATEGORY_INDEX_REGION) {
				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).addClass('selectedButton').removeClass('unSelectedButton');
				  
				}
				mapHandler.selectRegionArray(idArray);
			} else {
				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).addClass('selectedButton').removeClass('unSelectedButton');
				  
				}
				
			}
		
			
			
		}
		
	},
	mapHandler = {
		
		ypositions:[20, 80],
		clicked:function(index) {
			var cat = row0[CATEGORY_INDEX_REGION];
			
			var selectedColIndex = cat.startIndex + index;
			buttonManager.clicked(selectedColIndex);
			
			
			
			
		},
		unselectRegionArray:function(idArray) {
			var cat = row0[CATEGORY_INDEX_REGION];
			var mapIndex = 0;
			for(var i = 0; i <idArray.length; i++) {
				mapIndex = idArray[i] - cat.startIndex;
				$('#map-country'+mapIndex).css('top', this.ypositions[mapIndex]);
				  
			}
			
		},
		selectRegionArray:function(idArray) {
			var cat = row0[CATEGORY_INDEX_REGION];
			for(var i = 0; i <idArray.length; i++) {
				mapIndex = idArray[i] - cat.startIndex;
				$('#map-country'+mapIndex).css('top', '-2000px');
				  
			}
			
		}
		
		
	}
	focusManager = {
		
		setUpFocus:function() {
			
			
			
			
			
		}
		
		
	}
	cycleHandler = {
		
		focusByIndex:function(index) {
			$("div.slides").cycle(index);
			
			
		}
		
		
	}
	sectionHandler = {
		sectionLabels:['frequency', 'assetclass', 'segment'],
		resetSectionSelection:function(label) {
			var catIndex = this.getCatIndexFromLabel(label);
			if(catIndex == 0) {
				return;
			}
			var cat = row0[catIndex];
			if(cat.validSection == 0) {
				return;
			}
			var indexArr = [];
			for(var j = cat.startIndex; j <= cat.endIndex; j++) {
			  colObjArray[j].selected = 1;
				indexArr.push(j);
			}
			graficButtons.setUnselected(indexArr, catIndex);
			buttonManager.displayRows();
		},
		getCatIndexFromLabel:function(label) {
			if(label == 'frequency') {
				return CATEGORY_INDEX_FREQUENCY;
			} else if(label == 'assetclass') {
				return CATEGORY_INDEX_ASSETCLASS;
			} else if(label == 'segment') {
				return CATEGORY_INDEX_SEGMENT;
			}
			return 0;
		},
		resetAllSections:function() {
			var indexArr = [];
			var label = '';
			for(var i = 0; i < this.sectionLabels.length; i++) {
				label = this.sectionLabels[i];
				indexArr = [];
			  var catIndex = this.getCatIndexFromLabel(label);
			  if(catIndex == 0) {
				  continue;
			  }
			  var cat = row0[catIndex];
			  if(cat.validSection == 0) {
				  continue;
			  }
			
			  for(var j = cat.startIndex; j <= cat.endIndex; j++) {
			    colObjArray[j].selected = 1;
				  indexArr.push(j);
			  }
				graficButtons.setUnselected(indexArr, catIndex);
			}
			regionCategoryManager.selectAll();
			
			
		}
	}

});

String.prototype.truncate = function(){
    var re = this.match(/^.{0,15}[\S]*/);
    var l = re[0].length;
    var re = re[0].replace(/\s$/,'');
    if(l < this.length)
        re = re + "&hellip;";
    return re;
}

var row0Counter = 0;
var buttonManager = null;
var categoryManager = null;
var regionCategoryManager = null;
var graficButtons = null;
var mapHandler = null;
var focusManager = null;
var cycleHandler = null;
var sectionHandler = null;
var colObjArray = null;
var rows = null;
var row0 = null;
var allRows = null;
var globalSelectedRows = null;
var columnsCount = 0;
var displayStarted = false;


var LATEST_REWARD_INDEX = 5;
var POPULAR_REWARD_INDEX = 4;
var TITLE_REWARD_INDEX = 3;
var DESCRIPTION_REWARD_INDEX = 2;
var PDF_REWARD_INDEX = 1;
var PDF_LINK_INDEX = 2;

var CATEGORY_INDEX_FREQUENCY = 6;
var CATEGORY_INDEX_REGION = 5;
var CATEGORY_INDEX_ASSETCLASS = 4;
var CATEGORY_INDEX_SEGMENT = 3;
var CATEGORY_INDEX_LANGUAGE = 9;

var REGION_GLOBAL_BUTTON_OFFSET = 5;
var REGION_EUROPA_BUTTON_OFFSET = 1;
var REGION_SWITZERLAND_BUTTON_OFFSET = 0;









