$(document).ready(function() {

  $.ajax({
    type: "GET",
	  url: "/ubs_g15_cio_shelf_v2.xml",
	  dataType: "xml",
	  success: function(xml) {
			
      var $xml = $(xml);
      var title = $xml.find("Author").text();
			
		  rows = $xml.find("Row");
			var cells = $(rows[0]).children();

			row0 = [];
			row1 = [];
			var startIndex = 0;
			var endIndex = 0;
			var merges = 0;
			var na = '';
			$(rows[0]).children().each(function() {

				if($(this).attr('ss:MergeAcross')) {
			    merges = parseInt($(this).attr('ss:MergeAcross'));
				} else {
				
					merges = 0;
				}
				
				endIndex = startIndex +merges;
				na = $(this).find('Data').text();
				var validSection = 0;
				if(na.length > 0 && merges > 0) {
					validSection = 1;
				}
			
			 	row0.push({name:na,  merge:merges, selectedButtons:[], categoryIndex:row0Counter, startIndex:startIndex, endIndex:endIndex, validSection:validSection, selectionChecked:0});
				startIndex = endIndex;
				startIndex++;
				
				
			  row0Counter++;
			});
			var languageCat = row0[CATEGORY_INDEX_LANGUAGE];

			$(rows[1]).children().each(function() {
				
				
			  	row1.push($(this).find('Data').text());
				
				
			});
			columnsCount = row1.length;

			var sectionTable;
			var x = '';
			var j = 0;
			
			colObjArray = [];
			var assetClassAppendString = '';
			var frequencyAppendString = '';
			var segmentAppendString = '';
			var regionAppendString = '';
			for(var i = 0; i < row0.length; i++) {
				var colObj = null;
				var na = row0[i].name;
				var mer = row0[i].merge;
				
				if(na.length > 0 && mer > 0) {
					mer++;
					var buttonContainer = null;
					
					for(var ii=0; ii < mer; ii++) {
						
						colObj = {colIndex:j, name: row1[j], cat:i, selected:1}
						colObjArray.push(colObj);
						if(i == 3) {
							segmentAppendString +=   '<li id="list'+j+'" class="selectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
						} else	if(i == 4) {
							assetClassAppendString += '  <li id="list'+j+'" class="selectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
							//$('#assetClassButtons').append('  <li><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')">'+colObj.name+'</a></li>');
						} else if(i == 6) {
							frequencyAppendString += '  <li id="list'+j+'" class="selectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
						} else if(i == CATEGORY_INDEX_REGION) {
							
							regionAppendString += '  <li id="list'+j+'" class="selectedButton"><table style="height:100%; width:100%;"><tr><td valign="middle"><a id="button'+j+'" href="javascript:buttonManager.clicked('+ j +')" class="filterButton">'+colObj.name+'</a></td></tr></table></li>';
						}
						
						
						j++;
						
					}
					
					
				} else {
					colObj = {colIndex:j, name: row1[j], cat:i, selected:1}
					colObjArray.push(colObj);
					j++;
				}
				
				
				
			}
			//$('#segmentButtons').append(segmentAppendString);
			$('#assetClassButtons').append(assetClassAppendString);
			$('#frequencyButtons').append(frequencyAppendString);
			$('#regionButtonsSub').append(regionAppendString);
			
			allRows = [];
			var rowArr = null;
			var inFocusRows = [];
      var latestRows = [];
      var popularRows = [];
			
			var isInFocus = false;
			var latest = false;
			var popular = false;
			var inFocusIndex = 0;
			var languagesString;
			for(var iii= 0; iii < rows.length; iii++) {
				languagesString = [];
				isInFocus = false;
			  latest = false;
			  popular = false;
				var validRow = false;
				var href = '';
				rowArr = [];
				$(rows[iii]).children().each(function(index) {
				  var data = $(this).find('Data').text();
					// check for row validity
					if(index == PDF_LINK_INDEX) {
						//data = iii;
						if((data.length > 0)&& (data != '?')) {
							href = data;
							
							validRow = true;
						}
						
					}
					
			  	rowArr.push(data);
					if(index == (columnsCount - LATEST_REWARD_INDEX)) {
						if(data == 'x') {
							latest = true;
							
						}
					}
					if(index == (columnsCount - POPULAR_REWARD_INDEX)) {
						
						if(data == 'x') {
							
							popular = true;
						}
					}
					
					
				
				
			  });
				if(validRow) {
					
					for(var ilc = languageCat.startIndex; ilc <= languageCat.endIndex; ilc++) {
						if(rowArr[ilc] == 'x') {
							languagesString.push(row1[ilc]);
						}
					}
					//rowArr.push(href);
					rowArr.push(languagesString.join(', '))
				  allRows.push(rowArr);
					
					
				
					if(latest) {
						latestRows.push({row:rowArr, focusIndex:inFocusIndex});
					}
					if(popular) {
						popularRows.push({row:rowArr, focusIndex:inFocusIndex});
						
					}
					if(latest || popular) {
						inFocusRows.push(rowArr);
						inFocusIndex++;
					}
				}
				
			}

			globalSelectedRows = [];
			buttonManager.displayRows();
			/*
			var focusString = '';
			var slideThumbsString = '';
			
			var truncatedDesc = '';
			var descriptionString ='';
			for(var i = 0; i < inFocusRows.length; i++) {
				
				
					descriptionString = inFocusRows[i][columnsCount - DESCRIPTION_REWARD_INDEX ];

					if((undefined != descriptionString) && (descriptionString.length > 0)) {
					  descriptionString = descriptionString.replace(/\[a (.*?)\]/g, "<a $1 class=\"innerlink\">");
						descriptionString = descriptionString.replace(/\[\/a\]/g, "</a>");
						
				  }
				
				focusString += '<div>'+
                  '<h3>'+ inFocusRows[i][columnsCount -TITLE_REWARD_INDEX]+'</h3>'+
                  '<p class="focusdescription">'+
                    '<img src="images/pdf-images/'+ inFocusRows[i][PDF_LINK_INDEX] +'.jpg" class="right" />'+
                    descriptionString+  
                  '</p>'+
                  '<a href="/pdf/'+inFocusRows[i][PDF_LINK_INDEX]+'.pdf" class="download-button rounded" title="'+inFocusRows[i][(columnsCount - PDF_REWARD_INDEX)]+'">Download</a>'+
                  '<div class="tags">'+inFocusRows[i][columnsCount]+'</div>'+
                  '<div class="break"></div>'+
                '</div>';
								
				truncatedDesc = inFocusRows[i][columnsCount -DESCRIPTION_REWARD_INDEX].truncate();
				
				slideThumbsString += '<div>'+
                    '<img src="images/pdf-thumbs/'+ inFocusRows[i][PDF_LINK_INDEX] +'.jpg" />'+
                    '<h4><a href="#">'+inFocusRows[i][columnsCount -TITLE_REWARD_INDEX]+'</a></h4>'+
                    '<p>'+truncatedDesc+'</p>  '+                
                  '</div>';
				
				
				
			}
			
			
			$('#slides_div').append(focusString);
			$('#slides-content-div').append(slideThumbsString);
			
			
			var latestString = '';
			var latestRow = null;
			var focusIndex = 0;
			for(var i = 0; i < latestRows.length; i++) {
				latestRow = latestRows[i].row;
				focusIndex = latestRows[i].focusIndex;
				truncated = latestRow[columnsCount -DESCRIPTION_REWARD_INDEX].truncate();
				latestString += '<div class="box">'+
                  '<img src="/images/pdf-thumbs/'+ latestRow[PDF_LINK_INDEX] +'.jpg" />'+
                  '<h4><a href="javascript:cycleHandler.focusByIndex('+focusIndex+');">'+latestRow[columnsCount -TITLE_REWARD_INDEX]+'</a></h4>'+
                  '<p>'+truncated+'</p>'+
                '</div>';
			}
			latestString += '<div style="clear:both;"></div>';
			$('#latest-div').append(latestString);
			
			var popularString = '';
			var popularRow = null;
			for(var i = 0; i < popularRows.length; i++) {
				popularRow = popularRows[i].row;
				focusIndex = popularRows[i].focusIndex;
				truncated = popularRow[columnsCount -DESCRIPTION_REWARD_INDEX].truncate();
				popularString += '<div class="box">'+
                  '<img src="/images/pdf-thumbs/'+ latestRow[PDF_LINK_INDEX] +'.jpg" />'+
                  '<h4><a href="javascript:cycleHandler.focusByIndex('+focusIndex+');">'+popularRow[columnsCount -TITLE_REWARD_INDEX]+'</a></h4>'+
                  '<p>'+truncated+'</p>'+
                '</div>';
			}
			popularString += '<div style="clear:both;"></div>';
			
			$('#popular-div').append(popularString);
			
			
			
			$(".slide-thumbs .prev").click(function() {
        $("div.slides").cycle("prev");
			
			
			
			
       return false;
      });
      $(".slide-thumbs .next").click(function() {
         $("div.slides").cycle("next");
         return false;
      });
 
      $("div.slides").cycle({ 
        fx:    'scrollHorz',
        speed: 200,
        timeout: 0,
        pager:  '#slidesNav',
        startingSlide: 0,
        before: function(currSlideElement, nextSlideElement, options, forwardFlag) {
          $(".slides-content").animate({
            left: ( -(options.nextSlide * 292) + 160)
          }, 200);                                                
        } 
      });
			*/
			
      
    }
		
  });
	buttonManager = {
		
		mapClicked:function(index) {
			categoryManager.setButtonClicked(index);
			this.displayRows();
			
		},
		
		
		clicked:function(index) {
			mapMovieHandler.setSelected(index);
			categoryManager.setButtonClicked(index);
			this.displayRows();
		},
		displayRows:function() {
			globalSelectedRows = [];
			var currentRow = null;
			var chance = true;
			var selectionFound = false;
			var i = 0;
			for(var ii = 0; ii < allRows.length; ii++) {
				currentRow = allRows[ii];
				
				chance = true;
				var cat = null;
				for(i = 0; i < row0.length; i++) {
				  cat = row0[i];
					
					selectionFound = false;
				
					if(cat.validSection == 1 && ( cat.categoryIndex == CATEGORY_INDEX_ASSETCLASS  || cat.categoryIndex == CATEGORY_INDEX_FREQUENCY  || cat.categoryIndex == CATEGORY_INDEX_REGION)) {
						
						
						if(cat.selectionChecked == 0) {
              
						  for(var j = cat.startIndex; j <= cat.endIndex; j++) {
							  if(colObjArray[j].selected == 1) {

									cat.selectedButtons.push(j);
								  
								}
								
							}
						
							cat.selectionChecked = 1;
							
							
						}
						
						if(cat.selectedButtons.length > 0) {
						 	selectionFound = false;
							for(var iii = 0; iii < cat.selectedButtons.length; iii++) {
								if(currentRow[cat.selectedButtons[iii]] == 'x') {
									selectionFound = true;
									
									
								}
								
								
							}
							if(!selectionFound) {
								chance = false;
								
								i = row0.length;  // works as a break statement
								
							}
							
							
						}
						
					}
					
					
					
					
				}
				if(chance) {
				  globalSelectedRows.push(currentRow);
			  }
				
				
				
			}
	
			falseArray = [];
			for(i = 0; i < row0.length; i++) {
			  cat = row0[i];
				if(cat.validSection == 1) {
					cat.selectionChecked = 0;
					cat.selectedButtons = [];
				}
					
			}
			

			if(globalSelectedRows.length < 1) {
				$('#rightColumnContentList').html('No publications found for the current criteria');
				
			} else {
			  
			  var rowsString = '<ul id="publicationList">';
        var x = globalSelectedRows.length;
			  var descriptionString = '';
		
			  for(i =0; i < globalSelectedRows.length; i++) {
					currentRow = globalSelectedRows[i];
					
					
					descriptionString = currentRow[columnsCount - DESCRIPTION_REWARD_INDEX ];

					if((undefined != descriptionString) && (descriptionString.length > 0)) {
					  descriptionString = descriptionString.replace(/\[a (.*?)\]/g, "<a $1 class=\"innerlink\">");
						descriptionString = descriptionString.replace(/\[\/a\]/g, "</a>");
						
				  }
				  
					
					rowsString += '<li><table class="list-table"><tr><td valign="top" width="160">' +
                '    <img class="table_image" src="/images/pdf-preview/'+ currentRow[PDF_LINK_INDEX] +'.jpg">'+
								'  </td><td valign="top"><div class="list-content"><h3>'+currentRow[columnsCount - TITLE_REWARD_INDEX ]+'</h3>'+
                   descriptionString+
	 								'  <div class="document-languages">Languages: '+ currentRow[columnsCount]+'</div>'+
                '  <a class="download-button rounded" href="/pdf/'+currentRow[PDF_LINK_INDEX]+'.pdf" title="'+currentRow[(columnsCount - PDF_REWARD_INDEX)]+'">Download</a>'+
                '  </div></td></tr></table></li>';
					
				  
			  }
				
				
			  
			  rowsString += '</ul>';
			  
				
			  $('#rightColumnContentList').html(rowsString);
			  
			  
			}
			
			
			
			
			
			setOpenParts('list');
			
			
			
			
			
			
			
			
		}
		
		
		
		
		
		
	}
	categoryManager = {
		
		setButtonClicked:function(id) {
			catIndex = colObjArray[id].cat;
			
			cat = row0[catIndex];
			
			if(cat.validSection == 0) {
				return;
			}
			if(cat.categoryIndex == CATEGORY_INDEX_REGION) {
				
				regionCategoryManager.setButtonClicked(id);
				
				return;
			}
			
			
			
			
			var hasUnselectedButton = false;
			
			for(var j = cat.startIndex; j <= cat.endIndex; j++) {
				
			  if(colObjArray[j].selected == 0) {

					
				  hasUnselectedButton = true;
					break;
				}
								
			}
			if(hasUnselectedButton) {
				if(colObjArray[id].selected == 1) {
					
					colObjArray[id].selected = 0;
					var hasSelectedButton = false;
					for(j = cat.startIndex; j <= cat.endIndex; j++) {
						if(colObjArray[j].selected == 1) {
							hasSelectedButton = true;
						} 
					}
					
					if(hasSelectedButton) {
						graficButtons.setUnselected([id], catIndex);
					} else {
						var idArray = [];
						for(j = cat.startIndex; j <= cat.endIndex; j++) {
							colObjArray[j].selected = 1;
					    idArray.push(j);
						}
						graficButtons.setSelected(idArray, catIndex);
					}
					
				} else {
					colObjArray[id].selected = 1;
					graficButtons.setSelected([id], catIndex);
				}
			} else {
				
				var idArray =  [];
				for(var j = cat.startIndex; j <= cat.endIndex; j++) {
					
				  if(j != id) {
						colObjArray[j].selected = 0;
						idArray.push(j);
					}
					
					
				}
				
				graficButtons.setUnselected(idArray, catIndex);
			}
			
			
			
		}
		
		
		
	}
	regionCategoryManager = {
		
		selectAll:function() {
			var cat = row0[CATEGORY_INDEX_REGION];
			var globalIndex = cat.startIndex + REGION_GLOBAL_BUTTON_OFFSET;
			buttonManager.clicked(globalIndex);
			
		},
		
		
		
		setButtonClicked:function(id) {
			
			
			cat = row0[CATEGORY_INDEX_REGION];
			
			
			
			
			var hasUnselectedButton = false;
			
			for(var j = cat.startIndex; j <= cat.endIndex; j++) {
				
			  if(colObjArray[j].selected == 0) {

					
				  hasUnselectedButton = true;
					break;
				}
								
			}
			if(hasUnselectedButton) {
				if(colObjArray[id].selected == 1) {
					
					if(id == (cat.startIndex + REGION_GLOBAL_BUTTON_OFFSET)) {
						var idArray = [];
						for(j = cat.startIndex; j <= cat.endIndex; j++) {
							colObjArray[j].selected = 1;
					    idArray.push(j);
						}
						graficButtons.setSelected(idArray, CATEGORY_INDEX_REGION);
						
						
					} else {
					
					  colObjArray[id].selected = 0;
					  var hasSelectedButton = false;
					  for(j = cat.startIndex; j <= cat.endIndex; j++) {
						  if(colObjArray[j].selected == 1) {
							  hasSelectedButton = true;
						  } 
					  }
					
					  if(hasSelectedButton) {
						  graficButtons.setUnselected([id], CATEGORY_INDEX_REGION);
					  } else {
						  var idArray = [];
						  for(j = cat.startIndex; j <= cat.endIndex; j++) {
							  colObjArray[j].selected = 1;
					      idArray.push(j);
						  }
						  graficButtons.setSelected(idArray, CATEGORY_INDEX_REGION);
					  }
						
						
					}
				} else {
					
					colObjArray[id].selected = 1;
					var idArray = [];
					if(id == (cat.startIndex + REGION_GLOBAL_BUTTON_OFFSET)) {
						
						for(j = cat.startIndex; j <= cat.endIndex; j++) {
						 colObjArray[j].selected = 1;
					    idArray.push(j);
					  }
					  graficButtons.setSelected(idArray, CATEGORY_INDEX_REGION);
						
					} else {
					
					  graficButtons.setSelected([id], CATEGORY_INDEX_REGION);
					  for(var j = cat.startIndex; j <= cat.endIndex; j++) {
					
				      if(j != id) {
						    colObjArray[j].selected = 0;
					      idArray.push(j);
				      }
					
					
			      }
					
				    graficButtons.setUnselected(idArray, CATEGORY_INDEX_REGION);
					  
					}
				}
			} else {
				
				if(id != (cat.startIndex + REGION_GLOBAL_BUTTON_OFFSET)) {
				  var idArray =  [];
				  
						
				  for(var j = cat.startIndex; j <= cat.endIndex; j++) {
					
				    if(j != id) {
						  colObjArray[j].selected = 0;
					    idArray.push(j);
				    }
					
					
			    }
					
				
				  graficButtons.setUnselected(idArray, CATEGORY_INDEX_REGION);
			  }
			}
			
			
			
		}
		
		
	}
	
	
	graficButtons = {
		setUnselected:function(idArray, catIndex) {
			
			cat = row0[catIndex];
			if(catIndex == CATEGORY_INDEX_REGION) {
				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).removeClass('selectedButton').addClass('unSelectedButton');
				  
				}
				//mapHandler.unselectRegionArray(idArray);
			} else {

				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).removeClass('selectedButton').addClass('unSelectedButton');
				  
				}
			}
		},
		setSelected:function(idArray, catIndex) {
			if(catIndex == CATEGORY_INDEX_REGION) {
				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).addClass('selectedButton').removeClass('unSelectedButton');
				  
				}
				mapHandler.selectRegionArray(idArray);
			} else {
				for(var i = 0; i <idArray.length; i++) {
					$('#list'+idArray[i]).addClass('selectedButton').removeClass('unSelectedButton');
				  
				}
				
			}
			
			
		}
		
	},
	mapHandler = {
		
		ypositions:[20, 80],
		clicked:function(index) {
			var cat = row0[CATEGORY_INDEX_REGION];
			
			var selectedColIndex = cat.startIndex + index;
			buttonManager.clicked(selectedColIndex);
			
			
			
			
		},
		unselectRegionArray:function(idArray) {
			var cat = row0[CATEGORY_INDEX_REGION];
			var mapIndex = 0;
			for(var i = 0; i <idArray.length; i++) {
				mapIndex = idArray[i] - cat.startIndex;
				$('#map-country'+mapIndex).css('top', this.ypositions[mapIndex]);
				  
			}
			
		},
		selectRegionArray:function(idArray) {
			var cat = row0[CATEGORY_INDEX_REGION];
			for(var i = 0; i <idArray.length; i++) {
				mapIndex = idArray[i] - cat.startIndex;
				$('#map-country'+mapIndex).css('top', '-2000px');
				  
			}
			
		}
		
		
	}
	focusManager = {
		
		setUpFocus:function() {
			
			
			
			
			
		}
		
		
	}
	cycleHandler = {
		
		focusByIndex:function(index) {
			$("div.slides").cycle(index);
			
			
		}
		
		
	}
	sectionHandler = {
		
		resetSectionSelection:function(label) {
			var catIndex = this.getCatIndexFromLabel(label);
			if(catIndex == 0) {
				return;
			}
			var cat = row0[catIndex];
			
			if(cat.validSection == 0) {
				return;
			}
					
			
			var indexArr = [];
			for(var j = cat.startIndex; j <= cat.endIndex; j++) {
				
			  colObjArray[j].selected = 1;

				indexArr.push(j);
								
			}
			graficButtons.setSelected(indexArr, catIndex);
			
			buttonManager.displayRows();
			
			
			
		},
		getCatIndexFromLabel:function(label) {
			if(label == 'frequency') {
				return CATEGORY_INDEX_FREQUENCY;
			
			} else if(label == 'assetclass') {
				return CATEGORY_INDEX_ASSETCLASS;
			} else if(label == 'segment') {
				
				return CATEGORY_INDEX_SEGMENT;
			}
			
			return 0;
			
		}
		
		
		
	}

});

String.prototype.truncate = function(){
    var re = this.match(/^.{0,15}[\S]*/);
    var l = re[0].length;
    var re = re[0].replace(/\s$/,'');
    if(l < this.length)
        re = re + "&hellip;";
    return re;
}

var row0Counter = 0;
var buttonManager = null;
var categoryManager = null;
var regionCategoryManager = null;
var graficButtons = null;
var mapHandler = null;
var focusManager = null;
var cycleHandler = null;
var sectionHandler = null;
var colObjArray = null;
var rows = null;
var row0 = null;
var allRows = null;
var globalSelectedRows = null;
var columnsCount = 0;
var displayStarted = false;


var LATEST_REWARD_INDEX = 5;
var POPULAR_REWARD_INDEX = 4;
var TITLE_REWARD_INDEX = 3;
var DESCRIPTION_REWARD_INDEX = 2;
var PDF_REWARD_INDEX = 1;
var PDF_LINK_INDEX = 2;

var CATEGORY_INDEX_FREQUENCY = 6;
var CATEGORY_INDEX_REGION = 5;
var CATEGORY_INDEX_ASSETCLASS = 4;
var CATEGORY_INDEX_SEGMENT = 3;
var CATEGORY_INDEX_LANGUAGE = 9;

var REGION_GLOBAL_BUTTON_OFFSET = 5;
var REGION_EUROPA_BUTTON_OFFSET = 1;
var REGION_SWITZERLAND_BUTTON_OFFSET = 0;







