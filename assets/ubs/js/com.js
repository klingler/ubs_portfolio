$(function() {
  $(".slide-thumbs .prev").click(function() {
    $("div.slides").cycle("prev");
    return false;
  });
  $(".slide-thumbs .next").click(function() {
    $("div.slides").cycle("next");
    return false;
  });
 
  $("div.slides").cycle({ 
        fx:    'scrollHorz',
        speed: 200,
        timeout: 0,
        pager:  '#slidesNav',
        startingSlide: 2,
        before: function(currSlideElement, nextSlideElement, options, forwardFlag) {
          $(".slides-content").animate({
            left: ( -(options.nextSlide * 292) + 160)
          }, 200);                                                
        } 
      });
});