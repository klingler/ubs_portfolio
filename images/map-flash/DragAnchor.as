class DragAnchor extends MovieClip {
  private var scaleState = 'small';
  private var name:String;
  public function beginDrag() {
    
    if(this.scaleState == 'big') {
      
      this.startDrag(false,-711, -362,0 , 0 );
    }
    
  }
  
  public function onMouseDown() {
    var d = new Date();
    
    _root.mousetime  = d.getTime();

    this.beginDrag();
    
    
  }
  public function onMouseUp() {
    this.stopDrag();
    
    
    
  }
  public function setScaleState(st) {
    if(st == 'big') {
      this._x = -355;
      this._y = -181;
      this._xscale =200;
      
      this._yscale =200;
      this.scaleState = 'big';
    } else if(st == 'small') {
      this._xscale =100;
      
      this._yscale =100;
      this._x = 0;
      this._y = 0;
      this.scaleState = 'small';
    }
    
    
    
  }
  
  
  
  
}